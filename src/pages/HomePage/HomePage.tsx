import React, { useState } from "react";
import style from "./HomePage.module.scss";
import owl from '../../assets/imgHomePage/owl.svg';
import Union from '../../assets/imgHomePage/Union.svg';
// import videoplayback from '../../assets/imgHomePage/videoplayback.MP4'
import Video from '../../assets/imgHomePage/Video.svg';
import { homePageArray } from '../../utils/HomePageArray/HomePageArray';

export const HomePage = () => {
    const [active, setActive] = useState();

    return (
        <div className={style.homePage}>
            <div>
                <div className={style.homePage__firstBlock}>
                    <div className={style.homePage__firstBlock_img}>
                        <div className={style.homePage__firstBlock_left}>
                            <span className={style.homePage__firstBlock_text}>Образовательная платформа <br />ХОД Future Academy</span>
                            <p className={style.homePage__firstBlock_miniText}>Актуальные знания для новичков и профессионалов</p>
                        </div>
                    </div>
                </div>
                <div className={style.homePage__secondBlock}>
                    <div className={style.homePage__blockComment}>
                        <img src={Union} alt="" />
                        <p className={style.homePage__blockComment_text}>Кем вы хотите стать?
                            <br /><br />Пора найти себя
                            и выбрать подходящий курс :)
                            <br /><br />Удачи!</p>
                        <img src={owl} alt="owl" className={style.homePage__imgOwl} />
                    </div>
                    <div className={style.homePage__allGroupButtons}>
                        {homePageArray.map((item) =>
                            <div
                                className={style.homePage__groupButtons}

                            >
                                <img src={item.img} alt="" />
                                <div>
                                    <p className={style.homePage__titleGroupButtons}>{item.title}</p>
                                    <span className={style.homePage__contextGroupButtons}>{item.context}</span>
                                </div>
                            </div>)}
                    </div>
                </div>
                <div className={style.homePage__thirdBlock}>
                    {/* <video src="" className={style.homePage__thirdBlock_video}></video> */}
                    <img src={Video} alt="" className={style.homePage__thirdBlock_video} />
                    <p className={style.homePage__thirdBlock_text}>
                        Актуальные знания от признанных экспертов рынка
                        <br />для новичков и практикующих специалистов.
                    </p>
                    <div className={style.homePage__colorNunber_andText}>
                        <div >
                            <p className={style.homePage__colorNumber}>600</p>
                            <p className={style.homePage__andText}>Курсов</p>
                        </div>
                        <div >
                            <p className={style.homePage__colorNumber}>82</p>
                            <p className={style.homePage__andText}>Ведущих <br/>преподавателей</p>
                        </div>
                        <div >
                            <p className={style.homePage__colorNumber}>14 795</p>
                            <p className={style.homePage__andText}>Выпускников</p>
                        </div>
                    </div>
                </div>

            </div>
            <div>
            </div>
        </div>
    );
};

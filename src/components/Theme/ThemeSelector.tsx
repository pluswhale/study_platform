import React, { useState, useEffect } from "react";
import styled from "styled-components";
import _ from "lodash";
import { useTheme } from "../Theme/useTheme";
import { getFromLS } from "../../utils/ChangeThemeFn/storage";
import Dialog from "./Dialog";
import CreateThemeContent from "./CreateThemeContent";

const ThemedButton = styled.button`
  border: 0;
  display: inline-block;
  padding: 12px 10px;
  font-size: 14px;
  border-radius: 12px;
  margin-top: 5px;
  width: 100%;
  cursor: pointer;
`;

const Wrapper = styled.li`
  text-align: center;
  border-radius: 4px;
  border: 1px solid #000;
  list-style: none;
`;

const Container = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  margin-top: 3rem;
  width: 200px;
  padding: 10px;
`;

const Header = styled.h2`
  display: flex;
  justify-content: space-around;
`;

export const ThemeSelector = (props: any) => {
  const { setter } = props;
  const themesFromStore = getFromLS("all-themes");
  const [data, setData] = useState(themesFromStore.data);
  const [themes, setThemes] = useState<any>([]);
  const [showDialog, setShowDialog] = useState(false);
  const [newTheme, setNewTheme] = useState();

  const { setMode } = useTheme();

  const themeSwitcher = (selectedTheme: any) => {
    console.log(selectedTheme);
    setMode(selectedTheme);
    setter(selectedTheme);
  };

  useEffect(() => {
    setThemes(_.keys(data));
  }, [data]);

  useEffect(() => {
    props.newTheme && updateThemeCard(props.newTheme);
  }, [newTheme]);

  const updateThemeCard = (theme: any) => {
    const key = _.keys(theme)[0];
    const updated = { ...data, [key]: theme[key] };
    setData(updated);
  };

  const manageDialog = () => {
    setShowDialog(!showDialog);
  };

  const createTheme = (newTheme: any) => {
    console.log(newTheme);
    setShowDialog(false);
    setNewTheme(newTheme);
  };

  const ThemeCard = (props: any) => {
    return (
      // <Wrapper
      //   style={{
      //     backgroundColor: `${data[_.camelCase(props.theme.name)].colors.body}`,
      //     color: `${data[_.camelCase(props.theme.name)].colors.text}`,
      //     fontFamily: `${data[_.camelCase(props.theme.name)].font}`,
      //   }}
      // >
      <>
        {/* <span>Нажмите на кнопку для выбора темы</span> */}
        <ThemedButton
          onClick={(theme) => themeSwitcher(props.theme)}
          style={{
            backgroundColor: `${
              data[_.camelCase(props.theme.name)].colors.button.background
            }`,
            color: `${data[_.camelCase(props.theme.name)].colors.button.text}`,
            fontFamily: `${data[_.camelCase(props.theme.name)].font}`,
            border: data[props.theme.name] !== "Dark" ? `1px solid black` : `0`,
          }}
        >
          {props.theme.name}
        </ThemedButton>
      </>

      // </Wrapper>
    );
  };

  return (
    <div>
      {/* <Header>Выберите тему</Header> */}
      <Container>
        <Dialog
          header="Создание темы"
          body={<CreateThemeContent create={createTheme} />}
          open={showDialog}
          callback={manageDialog}
        />
        {themes.length > 0 &&
          themes.map((theme: any) => (
            <ThemeCard theme={data[theme]} key={data[theme].id} />
          ))}
        <button className="btn" onClick={manageDialog}>
          Создать тему
        </button>
      </Container>
    </div>
  );
};

import React, { useState } from "react";
import { Link } from "react-router-dom";
import logoHeaderBlack from "../../assets/imgHeader/logoHeaderBlack.svg";
import iconPersonBlack from "../../assets/imgHeader/iconPersonBlack.svg";

import style from "./Header.module.scss";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import {
  ConainerExpandedThemePicker,
  ConainerThemeChanger,
} from "./HeaderStyles";
import { ThemeSelector } from "../Theme/ThemeSelector";
import { headerLinksArr } from "../../utils/HeaderLinks/headerLinks";
import { iteratorSymbol } from "immer/dist/internal";
import { Button } from "@mui/material";

export const Header = (props: any) => {
  const { themeSetter } = props;
  const [themePickerOpen, setThemePickerOpen] = useState<boolean>(false);

  return (
    <div>
      <div className={style.header}>
        <div className={style.header__wrapper}>
          <Link to="/">
            <img src={logoHeaderBlack} alt="logo" />
          </Link>
          <div className={style.header__links}>
            {headerLinksArr.map((icon) => (
              <Link className={style.header__link} to="/">
                {icon.hasOwnProperty("img") ? (
                  <img
                    src={icon.img}
                    alt="icon"
                    className={style.header__linkIcon}
                  />
                ) : null}
                {icon.title}
                {icon.hasOwnProperty("expandedIcon") ? (
                  <img src={icon.expandedIcon} alt="icon" />
                ) : null}
              </Link>
            ))}
            <button className={style.header__loginButton}>
              <img
                src={iconPersonBlack}
                alt="icon_person"
                className={style.icon_header}
              />
              Войти
            </button>
            <ConainerThemeChanger>
              <button
                style={{
                  width: "100%",
                  fontSize: "1.6rem",
                  height: "100%",
                }}
                onClick={() => setThemePickerOpen((prev) => !prev)}
              >
                <DarkModeIcon /> Сменить тему
              </button>
              {themePickerOpen ? (
                <ConainerExpandedThemePicker>
                  <ThemeSelector setter={themeSetter} />
                </ConainerExpandedThemePicker>
              ) : null}
            </ConainerThemeChanger>
          </div>
        </div>
      </div>
    </div>
  );
};

import React from "react";
import { createRoot } from "react-dom/client";
import App from "./App";
import "./index.scss";
import { setToLS } from "./utils/ChangeThemeFn/storage";
import * as themes from "./components/Theme/schema.json";

const container = document.getElementById("root")!;
const root = createRoot(container);

const Index = () => {
  //@ts-ignore
  setToLS("all-themes", themes.default);
  return <App />;
};

root.render(
  <React.StrictMode>
    <Index />
  </React.StrictMode>,
);

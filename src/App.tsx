import React from "react";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import { HomePage } from "./pages/HomePage/HomePage";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import { AllCoursesPage } from "./pages/AllCoursesPage/AllCoursesPage";
import { EventsPage } from "./pages/EventsPage/EventsPage";
import { BaseKnowledgePage } from "./pages/BaseKnowledgePage/BaseKnowledgePage";
import { CareerPage } from "./pages/CareerPage/CareerPage";
import { Header } from "./components/Header/Header";
import { Footer } from "./components/Footer/Footer";
import { useState, useEffect } from "react";
import styled, { ThemeProvider } from "styled-components";
import WebFont from "webfontloader";
import { GlobalStyles } from "./components/Theme/GlobalStyles";
import { useTheme } from "./components/Theme/useTheme";
import { ThemeSelector } from "./components/Theme/ThemeSelector";
import Dialog from "./components/Theme/Dialog";
import CreateThemeContent from "./components/Theme/CreateThemeContent";

const Container = styled.div`
  margin: 5px auto 5px auto;
`;

function App() {
  const { theme, themeLoaded, getFonts } = useTheme();
  const [selectedTheme, setSelectedTheme] = useState(theme);

  useEffect(() => {
    setSelectedTheme(theme);
  }, [themeLoaded]);

  useEffect(() => {
    WebFont.load({
      google: {
        families: getFonts(),
      },
    });
  });

  return (
    <>
      {themeLoaded && (
        <ThemeProvider theme={selectedTheme}>
          <GlobalStyles />
          <Container style={{ fontFamily: selectedTheme.font }}>
            <Provider store={store}>
              <BrowserRouter>
                <Header
                  selectedTheme={selectedTheme}
                  themeSetter={setSelectedTheme}
                />
                <Routes>
                  <Route path="/" element={<HomePage />} />
                  <Route path="/allcoursespage" element={<AllCoursesPage />} />
                  <Route path="/eventspage" element={<EventsPage />} />
                  <Route
                    path="/baseknowledgepage"
                    element={<BaseKnowledgePage />}
                  />
                  <Route path="/careerpage" element={<CareerPage />} />
                </Routes>
                <Footer />
              </BrowserRouter>
            </Provider>
          </Container>
        </ThemeProvider>
      )}
    </>
  );
}

export default App;

//Route - это компонент всшего порядка. path, element - пропсы

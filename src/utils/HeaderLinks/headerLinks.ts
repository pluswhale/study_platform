import iconAllContentBlack from "../../assets/imgHeader/iconAllContentBlack.svg";
import iconLocationBlack from "../../assets/imgHeader/iconLocationBlack.svg";
import iconVectorDownBlack from "../../assets/imgHeader/iconVectorDownBlack.svg";

export const headerLinksArr = [
  { id: 1, title: "Все курсы", img: iconAllContentBlack },
  { id: 2, title: "Мероприятия" },
  { id: 3, title: "База знаний" },
  { id: 4, title: "Карьера" },
  {
    id: 4,
    title: "Нижний Новгород",
    img: iconLocationBlack,
    expandedIcon: iconVectorDownBlack,
  },
  { id: 4, title: "8 800 950-33-98" },
];

import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'


const initialState = { } 

const BaseKnowledgePage = createSlice({
  name:  'baseKnowledgePageReducer',
  initialState,
  reducers: {
   
  },
})

export const {  } = BaseKnowledgePage.actions
export default BaseKnowledgePage.reducer


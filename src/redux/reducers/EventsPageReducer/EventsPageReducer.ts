import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'


const initialState = { } 

const EventsPage = createSlice({
  name:  'eventsPageReducer',
  initialState,
  reducers: {
   
  },
})

export const {  } = EventsPage.actions
export default EventsPage.reducer

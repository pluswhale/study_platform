import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'


const initialState = { } 

const AllCoursesPage = createSlice({
  name:  'allCoursesPage',
  initialState,
  reducers: {
   
  },
})

export const {  } = AllCoursesPage.actions
export default AllCoursesPage.reducer
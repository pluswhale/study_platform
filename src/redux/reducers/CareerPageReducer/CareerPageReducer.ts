import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'


const initialState = { } 

const CareerPage = createSlice({
  name:  'careerPageReducer',
  initialState,
  reducers: {
   
  },
})

export const {  } = CareerPage.actions
export default CareerPage.reducer

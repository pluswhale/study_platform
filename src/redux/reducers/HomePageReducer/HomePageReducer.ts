import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'


const initialState = { } 

const HomePage = createSlice({
  name:  'homePage',
  initialState,
  reducers: {
   
  },
})

export const {  } = HomePage.actions
export default HomePage.reducer

//createSlice - собирает редюсер, его начально состояние и его редюсеры
import { combineReducers, configureStore } from '@reduxjs/toolkit'
import AllCoursesPageReducer from './reducers/AllCoursesPageReducer/AllCoursesPageReducer'
import BaseKnowledgePageReducer from './reducers/BaseKnowledgePageReducer/BaseKnowledgePageReducer'
import CareerPageReducer from './reducers/CareerPageReducer/CareerPageReducer'
import EventsPageReducer from './reducers/EventsPageReducer/EventsPageReducer'
import HomePageReducer from './reducers/HomePageReducer/HomePageReducer'


export const reducer = combineReducers({
    HomePage: HomePageReducer,
    AllCoursesPage: AllCoursesPageReducer,
    BaseKnowledgePage: BaseKnowledgePageReducer,
    CareerPage: CareerPageReducer,
    EventsPage: EventsPageReducer,

})
export const store = configureStore({
    reducer,
    // middleware(getDefaultMiddleware) {

    // },

})




